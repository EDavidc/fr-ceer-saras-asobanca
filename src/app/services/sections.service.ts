import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class SectionsService {
  private readonly URL_BASE = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  //section/all
  getAllSection():Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/section/all`);
  }
}
