import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.development';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BanksService {

  private readonly URL_BASE = environment.apiUrl;

  
  constructor(
    private http: HttpClient
  ) { }

  getBanks():Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/getAllBanks`);
  }

  //register
  register(obj:any):Observable<any> {
    return this.http.post<any>(`${this.URL_BASE}/register`, obj);
  }
  //auth/login
  login(obj:any):Observable<any> {
    return this.http.post<any>(`${this.URL_BASE}/auth/login`, obj);
  }
  //user/update
  updateUser(obj:any):Observable<any> {
    return this.http.put<any>(`${this.URL_BASE}/user/update`, obj);
  }
  //user/updatePassword
  updatePassword(obj:any):Observable<any> {
    return this.http.put<any>(`${this.URL_BASE}/user/updatePassword`, obj);
  }
  //user/checkOldPassword
  checkOldPassword(obj:any):Observable<any> {
    return this.http.post<any>(`${this.URL_BASE}/auth/check/old/password`, obj);
  }

  //auth/recovery/password
  recoveryPassword(obj:any):Observable<any> {
    return this.http.post<any>(`${this.URL_BASE}/auth/recovery/password`, obj);
  }


  getUserInformation():Observable<any>{
    return this.http.get<any>(`${this.URL_BASE}/auth/me`);
  }

  //auth/checkIfUserExist
  checkIfUserExist(obj:any):Observable<any> {
    return this.http.post<any>(`${this.URL_BASE}/user/check`, obj);
  }
}
