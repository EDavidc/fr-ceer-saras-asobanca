import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.development';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalizationService {
  private readonly URL_BASE = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  //Provincias
  getProvinces():Observable<any>{
    return this.http.get<any>(`${this.URL_BASE}/provinces`);
  }

  //Cantones
  getCantons(provinceId:number):Observable<any>{
    return this.http.get<any>(`${this.URL_BASE}/cantons/${provinceId}`);
  }

  //Parroquias
  getParishes(cantonId:number):Observable<any>{
    return this.http.get<any>(`${this.URL_BASE}/parishes/${cantonId}`);
  }
}
