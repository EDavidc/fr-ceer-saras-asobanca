import { TestBed } from '@angular/core/testing';

import { ListaddService } from './listadd.service';

describe('ListaddService', () => {
  let service: ListaddService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListaddService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
