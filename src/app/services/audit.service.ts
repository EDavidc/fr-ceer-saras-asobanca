import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuditService {

  private readonly URL_BASE = environment.apiUrl;

  
  constructor(
    private http: HttpClient
  ) { }

  registerAudit(body:any):Observable<any> {
    return this.http.post<any>(`${this.URL_BASE}/audit/register`,body);
  }

  //GET IP
  getIpAddress():Observable<any> {
    return this.http.get<any>('https://api64.ipify.org/?format=json');
  }

  //PUT LAST REGISTER
  putRegister(obj:any):Observable<any> {
    return this.http.put<any>(`${this.URL_BASE}/audit/updateRegister`,obj);
  }

  detectBrowser(): string {
    const userAgent = window.navigator.userAgent;

    if (userAgent.indexOf('Chrome') !== -1) {
      return 'Google Chrome';
    } else if (userAgent.indexOf('Safari') !== -1) {
      return 'Safari';
    } else if (userAgent.indexOf('Firefox') !== -1) {
      return 'Mozilla Firefox';
    } else if (userAgent.indexOf('Edge') !== -1) {
      return 'Microsoft Edge';
    } else if (userAgent.indexOf('MSIE') !== -1 || userAgent.indexOf('Trident/') !== -1) {
      return 'Internet Explorer';
    } else {
      return 'Unknown Browser';
    }
  }

  //POST GENERATE REPORT
  generateReport(id:number):Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/audit/generateReport/${id}`);
  }

  //GET REPORT
  getReport(id:number):Observable<any>{
    return this.http.get<any>(`${this.URL_BASE}/getReport/${id}`);
  }

  //GET RIESGO TERRITORIAL
  getRiesgoTerritorial(id:number):Observable<any>{
    return this.http.get<any>(`${this.URL_BASE}/getRiesgo/${id}`);
  }

}
