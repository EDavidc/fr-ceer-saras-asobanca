import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  showAlert(title:string, message:string, type:string) {
    switch(type){
      case 'SUCCESS':
        this.toastr.success(message, title);
        break;
      case 'ERROR':
        this.toastr.error(message, title);
        break;
      case 'WARNING':
        this.toastr.warning(message, title);
        break;
      case 'INFO':
        this.toastr.info(message, title);
        break;
      default:
        this.toastr.info(message, title);
        break;
    }
  }

  generateUniqueCode(){
    let result = '';
    const characters = '0123456789';
    const charactersLength = characters.length;

    for (let i = 0; i < 6; i++) {
      const randomIndex = Math.floor(Math.random() * charactersLength);
      result += characters.charAt(randomIndex);
    }

    return result;
  }

  showSpinner(){
    this.spinner.show();
  }

  hideSpinner(){
    this.spinner.hide();
  }
}
