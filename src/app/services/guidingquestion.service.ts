import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class GuidingquestionService {
  private readonly URL_BASE = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  getAllGuidingQuestion(): Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/guidingquestion/all`);
  }
  //guides/all
  getGuidingQuestionById(idguia: number): Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/guidingquestion/getGuidingQuestionById/${idguia}`);
  }


}
