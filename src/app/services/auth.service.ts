import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  getToken():Observable<any>{
    const token = localStorage.getItem('token');
    return of(token);
  }

  isLoggedIn(){
    const token = localStorage.getItem('token');
    if(token){
      return true;
    }
    return false;
  }
}
