import { TestBed } from '@angular/core/testing';

import { GuidingquestionService } from './guidingquestion.service';

describe('GuidingquestionService', () => {
  let service: GuidingquestionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GuidingquestionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
