import { environment } from './../../environments/environment.development';
import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, WritableSignal, signal } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  private readonly URL_BASE = environment.apiUrl;

  userInformation = new BehaviorSubject({});
  codeValidation = new BehaviorSubject("");
  aggre = new BehaviorSubject("NOT_AGGRE");
  private isAuthenticatedSubject = new BehaviorSubject<boolean>(false);
  ipAddress = new BehaviorSubject<any>({});
  ip = new Subject<any>();
  count: WritableSignal<any> = signal({});

  $asObsagree = this.aggre.asObservable();


  constructor(
    private http: HttpClient
  ) 
  { }

  getTextAgreeById(id:number):Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/getTextAgree/${id}`);
  }
  //mail/verification
  sendMail(data:any):Observable<any> {
    return this.http.post<any>(`${this.URL_BASE}/mail/send`, data);
  }

  setAuthenticated(value: boolean): void {
    this.isAuthenticatedSubject.next(value);
  }

  isAuthenticated(): BehaviorSubject<boolean> {
    return this.isAuthenticatedSubject;
  }

}
