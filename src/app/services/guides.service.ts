import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class GuidesService {
  private readonly URL_BASE = environment.apiUrl;


  constructor(
    private http: HttpClient
  ) { }

  //guides/all
  getAllGuides():Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/guides/all`);
  }
  getGuideById(guideId:number):Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/guides/getGuideById/${guideId}`);
  }

  getPathDownloadGuide(id:number):Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/guides/downloadGuideById/${id}`);
  }
  getPathDownloadDoc(id:number):Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/guides/downloadDocById/${id}`);
  }
  ///guides/getInformationStaticReport/{guideId}/{dimensionId}
  getInformationStaticReport(guideId:number,dimensionId:number):Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/guides/getInformationStaticReport/${guideId}/${dimensionId}`);
  }
  getTititleStaticreport(guideId:number,dimensionId:number):Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/guides/titleStaticReport/${guideId}/${dimensionId}`);
  }
  getInformationDynamicReport(guideId:number):Observable<any> {
    return this.http.get<any>(`${this.URL_BASE}/guides/getInformationDynamicReport/${guideId}`);
  }

  getCuestionario(id:number):Observable<any>{
    return this.http.get<any>(`${this.URL_BASE}/getCuestionario/${id}`);
  }
}