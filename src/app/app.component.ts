import { CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AuditService } from './services/audit.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    NgxSpinnerModule
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: []
})
export class AppComponent {
  title = 'fr-ceer-saras-asobanca';
  /**
   *
   */
  constructor(
    private _auditService: AuditService
  ) {
    this._auditService.getIpAddress().subscribe(res => {
      sessionStorage.setItem('ip', res.ip);
    })
  }
}
