import { subscribe } from 'node:diagnostics_channel';
import { CanActivateFn, Router } from '@angular/router';
import { GeneralService } from '../services/general.service';
import { inject } from '@angular/core';

export const termsGuard: CanActivateFn = (route, state) => {
  const _generalService = inject(GeneralService);
  const router = inject(Router)
  
  const aggreeOK = _generalService.aggre.getValue();
  _generalService.aggre.subscribe({
    next:(err)=>{
      console.log("err",err);
    }
  })
  if(aggreeOK){
    return true;
  }
  return router.createUrlTree(['/']);
};
