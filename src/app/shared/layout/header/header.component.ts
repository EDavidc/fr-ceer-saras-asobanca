import { UserUpdateComponent } from './../../components/user-update/user-update.component';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ModalDialogModule, ModalDialogService } from 'ngx-modal-dialog';
import { ChangePasswordComponent } from '../../components/change-password/change-password.component';
import { Router } from '@angular/router';
import { BanksService } from '../../../services/banks.service';
import { UtilsService } from '../../../services/utils.service';
import { GeneralService } from '../../../services/general.service';
import { GUEST } from '../../../data/Constants';
import { NgIf } from '@angular/common';
declare var $: any; // Declare jQuery to avoid TypeScript errors

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [
    UserUpdateComponent,
    ChangePasswordComponent,
    NgIf
  ],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css',
  providers: [
    
  ]
})
export class HeaderComponent implements OnInit {

  userName!:string;
  userInfo!:any;
  typeAuth!:any;
  readonly TYPE_GUEST = GUEST;
  constructor(
   private router: Router,
   private _bankService: BanksService,
   private _utilService: UtilsService,
   private _generalService: GeneralService
  ) { }

  ngOnInit(): void {
    this.userName = localStorage.getItem("username")!;
    this.typeAuth = localStorage.getItem("type")
  }
  
  openModalUpdateInformation(){
   this.loadInformation();
  }

  openModalChangePassword(){

  }

  logout(){
    this.router.navigate(['/']);
    localStorage.clear();
  }

  menu(){
    this.router.navigate(['/guides']);
  }

  loadInformation(){
    this._utilService.showSpinner();
    this._bankService.getUserInformation().subscribe({
      next: (user)=>{
        this._generalService.userInformation.next(user);
        this.userInfo = user;
        $('#updateUserModal').modal('show');
        this._utilService.hideSpinner()
      }, error: (err)=>{
        console.log(err);
        this._utilService.hideSpinner()
      }
    })
  }
}
