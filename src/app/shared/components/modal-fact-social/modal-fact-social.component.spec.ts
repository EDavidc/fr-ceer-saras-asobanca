import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFactSocialComponent } from './modal-fact-social.component';

describe('ModalFactSocialComponent', () => {
  let component: ModalFactSocialComponent;
  let fixture: ComponentFixture<ModalFactSocialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ModalFactSocialComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModalFactSocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
