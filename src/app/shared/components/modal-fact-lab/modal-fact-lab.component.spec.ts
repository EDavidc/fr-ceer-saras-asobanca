import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFactLabComponent } from './modal-fact-lab.component';

describe('ModalFactLabComponent', () => {
  let component: ModalFactLabComponent;
  let fixture: ComponentFixture<ModalFactLabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ModalFactLabComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModalFactLabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
