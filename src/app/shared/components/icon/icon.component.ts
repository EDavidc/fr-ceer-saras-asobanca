import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'icon-component',
  standalone: true,
  imports: [
    CommonModule
  ],
  templateUrl: './icon.component.html',
  styleUrl: './icon.component.css'
})
export class IconComponent {
  @Input('icon') icon!:string;
  @Input('backgroundColor') backgroundColor!:string;
}
