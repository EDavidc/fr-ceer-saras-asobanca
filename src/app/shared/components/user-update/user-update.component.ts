import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { UtilsService } from '../../../services/utils.service';
import { NgIf } from '@angular/common';
import { BanksService } from '../../../services/banks.service';
import { HttpClientModule } from '@angular/common/http';
import { NgxMaskDirective, provideNgxMask } from 'ngx-mask';
import { GeneralService } from '../../../services/general.service';
declare var $: any; // Declare jQuery to avoid TypeScript errors

@Component({
  selector: 'app-user-update',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    NgIf,
    HttpClientModule,
    NgxMaskDirective
  ],
  templateUrl: './user-update.component.html',
  styleUrl: './user-update.component.css',
  providers: [
    UtilsService,
    BanksService,
    provideNgxMask()
  ]
})
export class UserUpdateComponent {

  @Input('user') user:any;
  generalNumber = {
    S: { pattern: new RegExp('[0-9]') },
    L: { pattern: new RegExp('[\\p{L}\\p{M}\\s]', 'u')}
  };

  formUpdateData!: FormGroup;
  showFooter = true;
  constructor(
    private fb: FormBuilder,
    private _utilService: UtilsService,
    private _bankService: BanksService,
    private _generalService: GeneralService
  ) {}

  ngOnInit() {
    console.log(this.user);
    this.initForm();
    this.loadInformation();
  }

  initForm() {
    this.formUpdateData =this.fb.group({
      name: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      phoneNumber: [null, [Validators.required]],
    });
  }

  updateUser(){
    if(this.formUpdateData.invalid) {
      this.formUpdateData.markAllAsTouched();
      return this._utilService.showAlert('Campos incompletos','Ingrese la información solicitada',"WARNING");
    }
    this.showFooter = false;
    const dataForm = this.formUpdateData.value
    const objUpdate = {
      p01_id: localStorage.getItem('p01_id'),
      p01_nombre: dataForm.name,
      p01_apellido: dataForm.lastName,
      p01_celular: dataForm.phoneNumber
    }
    this._bankService.updateUser(objUpdate).subscribe({
      next: res => {
        if(res.status ==='OK'){
          this.formUpdateData.reset();
          this._utilService.showAlert('Usuario actualizado', 'Usuario actualizado',"SUCCESS");
          this.showFooter = true;
          $('#updateUserModal').modal('hide');
        }else{
          this._utilService.showAlert('Error al actualizar usuario', 'Ha ocurrido un error, intentalo mas tarde',"ERROR");
          this.showFooter = true;
        }
      },
      error: err => {
        console.log(err);
        this.showFooter = true;
        this._utilService.showAlert('Error!', 'Ha ocurrido un error, intentalo mas tarde',"ERROR");
      }
    })
  }

  

  ngAfterViewInit(){
    console.log("ERR");
    
  }

  loadInformation(){
    this._generalService.userInformation.subscribe({
      next: (user:any)=>{
        if(user){
            this.formUpdateData.get('name')?.setValue(user.p01_nombre);
            this.formUpdateData.get('lastName')?.setValue(user.p01_apellido);
            this.formUpdateData.get('phoneNumber')?.setValue(user.p01_celular);
        }
      }
    })      
  }
}
