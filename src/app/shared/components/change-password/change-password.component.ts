import { Component, OnInit } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { BanksService } from '../../../services/banks.service';
import { UtilsService } from '../../../services/utils.service';
import { NgIf } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
declare var $: any; // Declare jQuery to avoid TypeScript errors

@Component({
  selector: 'app-change-password',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    NgIf,
    HttpClientModule
  ],
  templateUrl: './change-password.component.html',
  styleUrl: './change-password.component.css',
  providers: [
    UtilsService,
    BanksService
  ]
})
export class ChangePasswordComponent implements OnInit {

  formChangePassword: any;
  showFooter = true;
  msgUnmatched = false;
  /**
   *
   */
  constructor(
    private fb: FormBuilder,
    private _utilService: UtilsService,
    private _bankService: BanksService
  ) {
  }
  ngOnInit(): void {
    this.initForm();
  }

  updatePassword() {
    if (this.formChangePassword.invalid) {
      this.formChangePassword.markAllAsTouched();
      return this._utilService.showAlert('Campos incompletos', 'Ingrese la información solicitada', "WARNING");
    }
    this.showFooter = false;
    const dataForm = this.formChangePassword.value
    const objOldPassword = {
      p01_id: localStorage.getItem('p01_id'),
      password: dataForm.oldPassword
    }
    this._bankService.checkOldPassword(objOldPassword).subscribe({
      next: res => {
        if (res.status === 'SUCCESS') {
          const objUpdate = {
            p01_id: localStorage.getItem('p01_id'),
            password: dataForm.newPassword
          }
          this.sendUpdatePassword(objUpdate);
        } else if (res.status === 'ERROR') {
          this._utilService.showAlert('Contraseña incorrecta', 'La contraseña actual es incorrecta', "ERROR");
          this.showFooter = true;
        }
      }, error: err => {
        this.showFooter = true;
        this._utilService.showAlert('Error!', 'Ha ocurrido un error, intentalo mas tarde', "ERROR");
      }
    })
  }
  initForm() {
    this.formChangePassword = this.fb.group({
      oldPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
    });
  }

  sendUpdatePassword(objUpdate: any) {
    this._bankService.updatePassword(objUpdate).subscribe({
      next: res => {
        if (res.status === 'SUCCESS') {
          this.formChangePassword.reset();
          this._utilService.showAlert('Contraseña actualizada', 'Contraseña actualizada', "SUCCESS");
          this.showFooter = true;
          $('#changePasswordModal').modal('hide');
        } else {
          this._utilService.showAlert('Error al actualizar contraseña', 'Ha ocurrido un error, intentalo mas tarde', "ERROR");
          this.showFooter = true;
        }
      },
      error: err => {
        console.log(err);
        this.showFooter = true;
        this._utilService.showAlert('Error!', 'Ha ocurrido un error, intentalo mas tarde', "ERROR");
      }
    })
  }
  passwordMatchValidator() {
    const password = this.formChangePassword.get('newPassword')?.value;
    const confirmPassword = this.formChangePassword.get('confirmPassword')?.value;
    if(password !== confirmPassword){
      this.msgUnmatched = true;
    }else{
      this.msgUnmatched = false;
    }
  }
}
