import { NgIf } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-modal-fact-env',
  standalone: true,
  imports: [
    NgIf
  ],
  templateUrl: './modal-fact-env.component.html',
  styleUrl: './modal-fact-env.component.css'
})
export class ModalFactEnvComponent {

  @Input('title') title!:string;
  @Input('description') description!:string;

  close(){
    const modal = document.getElementById('emergentModal');
    if (modal) {
      modal.classList.remove('show');
      modal.style.display = 'none';
    }
  }
}
