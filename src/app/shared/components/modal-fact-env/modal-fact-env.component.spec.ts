import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFactEnvComponent } from './modal-fact-env.component';

describe('ModalFactEnvComponent', () => {
  let component: ModalFactEnvComponent;
  let fixture: ComponentFixture<ModalFactEnvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ModalFactEnvComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModalFactEnvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
