import { Component, OnInit, inject } from '@angular/core';
import { HeaderComponent } from '../../../shared/layout/header/header.component';
import { FooterComponent } from '../../../shared/layout/footer/footer.component';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { GeneralService } from '../../../services/general.service';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule, NgIf, NgStyle } from '@angular/common';
import { Router } from '@angular/router';
import { UtilsService } from '../../../services/utils.service';
import { STATUS_OK } from '../../../data/Constants';
import { AuditService } from '../../../services/audit.service';
@Component({
  selector: 'app-home',
  standalone: true,
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
  imports: [
    NgStyle,
    HeaderComponent,
    FooterComponent,
    ReactiveFormsModule,
    HttpClientModule,
    NgIf
  ],
  providers: [GeneralService, AuditService]
})
export class HomeComponent implements OnInit {
  objAgree: any;
  img = 'assets/images/background/Imagen_persona.png'
  _generalService = inject(GeneralService);

  /**
   *
   */
  constructor(
    // private _generalService: GeneralService,
    private fb: FormBuilder,
    private route: Router,
    private _utilService: UtilsService,
    private _auditService: AuditService
  ) {
  }
  ngOnInit(): void {
    this.initForm();
    this.detectChangeTypeUser();
    this.textAgree(1);
  }

  formHome!: FormGroup;
  redirSignIn() {
    const typeUser = this.formHome.get('typeUser')?.value;
    let ip = this._generalService.ipAddress.getValue();
      console.log(ip);
    if (!this.formHome.get('agree')?.value) {
      this._utilService.showAlert('Aceptar acuerdo de uso', 'Para poder continuar debe aceptar los acuerdos de uso.', 'WARNING')
      return;
    }
    
    if(this.formHome.get('agree')?.value){
        this._generalService.setAuthenticated(true);
        if(parseInt(typeUser) == 1){
          this.formHome.get('agree')?.setValue(false);
          this.route.navigateByUrl('/public-sign-in');
        }else if(typeUser == 5){
          this.formHome.get('agree')?.setValue(false);
          this.route.navigateByUrl('/sign-in');
        }      
    }    
  }

  initForm() {
    this.formHome = this.fb.group({
      typeUser: ['1'],
      agree: [false]
    });
  }

  detectChangeTypeUser() {
    this.formHome.get('typeUser')?.valueChanges.subscribe({
      next: (value) => {
        this.textAgree(value);
      }
    })
  }
  textAgree(id: number) {
    this._utilService.showSpinner();
    this._generalService.getTextAgreeById(id).subscribe(res => {
      if (res.status === STATUS_OK) {
        this.objAgree = res.data;
      }
      this._utilService.hideSpinner();
    })
  }

}
