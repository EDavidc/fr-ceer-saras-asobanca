import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuidingquestionComponent } from './guidingquestion.component';

describe('GuidingquestionComponent', () => {
  let component: GuidingquestionComponent;
  let fixture: ComponentFixture<GuidingquestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [GuidingquestionComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(GuidingquestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
