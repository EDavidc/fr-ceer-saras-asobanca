import { Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';
import { FooterComponent } from '../../shared/layout/footer/footer.component';
import { HeaderComponent } from '../../shared/layout/header/header.component';
import { NgFor, NgIf } from '@angular/common';
import { Router, RouterLink } from '@angular/router';
import { GuidingquestionService } from '../../services/guidingquestion.service';
import { SectionsService } from '../../services/sections.service';
import { UtilsService } from '../../services/utils.service';
import { Question } from '../../models/question';

@Component({
  selector: 'app-guidingquestion',
  standalone: true,
  imports: [
    FooterComponent,
    HeaderComponent,
    NgFor,
    RouterLink,
    NgIf
  ],
  templateUrl: './guidingquestion.component.html',
  styleUrl: './guidingquestion.component.css',
  providers: [
    SectionsService,
    GuidingquestionService
  ],
})



export class GuidingquestionComponent implements OnInit {

  constructor(
    private _sectionsService: SectionsService,
    private _guidingquestionService: GuidingquestionService,
    private router: Router,
    private _utilService: UtilsService,
  ) {
  }

  ngOnInit(): void {
    this.filterquestion(5);
  }

  catalogQuestion: any;
  questionSelect: any = [];



  filterquestion(idguia: any): any {
    this._guidingquestionService.getGuidingQuestionById(idguia)
      .subscribe((data1) => {
        if (data1.status === 'OK') {
          this.catalogQuestion = data1.data;
        }
      }
      )
  }

  save() {
    this.questionSelect;

  }

  checkValue(id: number, val: any) {
    const d = this.questionSelect.indexOf(id);

    if (d !== -1) {
      this.questionSelect.splice(d, 1);
    }

    if (val.checked) {
      this.questionSelect.push(id);
    }
  }
}
