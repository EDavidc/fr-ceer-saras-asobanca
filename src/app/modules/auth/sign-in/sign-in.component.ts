import { BanksService } from './../../../services/banks.service';
import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { UtilsService } from '../../../services/utils.service';
import { GeneralService } from '../../../services/general.service';
import { HttpClientModule } from '@angular/common/http';
import { NgIf } from '@angular/common';
import { STATUS_OK, NOT_FOUND } from '../../../data/Constants'
import { AuditService } from '../../../services/audit.service';
@Component({
  selector: 'app-sign-in',
  standalone: true,
  imports: [
    RouterLink,
    ReactiveFormsModule,
    HttpClientModule,
    NgIf
  ],
  templateUrl: './sign-in.component.html',
  styleUrl: './sign-in.component.css',
  providers: [
    BanksService,
    GeneralService,
    // AuditService
  ]
})
export class SignInComponent implements OnInit {

  showButtons = true;
  _generalService = inject(GeneralService);
  img = 'assets/images/background/guias-login.png';
  /**
   *
   */
  constructor(
    private fb: FormBuilder,
    private route: Router,
    private _utilService: UtilsService,
    private _bankService: BanksService,
    private _auditService: AuditService,
    // private _generalService: GeneralService
  ) {

  }
  ngOnInit(): void {
    this.initForm();
  }
  formSignIn!: FormGroup;
  signIn() {
    if (this.formSignIn.invalid) {
      this.formSignIn.markAllAsTouched();
      return this._utilService.showAlert('Campos incompletos', 'Ingrese la información solicitada', "WARNING");
    }
    this.showButtons = false;
    this._bankService.login(this.formSignIn.value).subscribe({
      next: res => {
        if (res.status === STATUS_OK) {
          localStorage.setItem('p01_id', res.data.user.p01_id);
          localStorage.setItem('token', res.data.access_token);
          localStorage.setItem('username', `${res.data.user.p01_nombre} ${res.data.user.p01_apellido}`);
          localStorage.setItem('email', res.data.user.email);
          localStorage.setItem('p05_id', res.data.user.p05_id);
          this._utilService.showAlert('Ingreso Exitoso', 'Bienvenido a CEER', "SUCCESS");
          this.insertAudit(res.data.user.p01_nombre, res.data.user.p01_apellido, res.data.user.email, res.data.user.p05_id, res.data.user.p01_id);
          this.route.navigate(['/guides']);
          this.showButtons = true;
        } else if (res.status === NOT_FOUND) {
          this._utilService.showAlert('Usuario no existe', 'El correo electrónico ingresado no se encuentra registrado en el sistema', "ERROR");
          this.showButtons = true;
        } else {
          this._utilService.showAlert('Error al iniciar sesión', 'Ha ocurrido un error, intentalo mas tarde', "ERROR");
          this.showButtons = true;
        }
      }
    }).add(() => {
      this.showButtons = true;
    });
  }
  initForm() {
    this.formSignIn = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

  menu(){
    this.route.navigate(['/']);
    localStorage.clear();
  }

  insertAudit(name: string, lastName: string, institution: string, email: string, p01_id: number) {
    const objAUdit = {
      p15_id: null,
      p01_id: p01_id,
      r01_nombre: name,
      r01_apellido: lastName,
      r01_institucion: institution,
      r01_correo: email,
      r01_ip: sessionStorage.getItem('ip'),
      r01_navegador: this._auditService.detectBrowser(),
    }
    this._generalService.ipAddress.subscribe(res => {
      console.log(res);
      
     }
    )
    
    this._auditService.registerAudit(objAUdit).subscribe({
      next: (audit: any) => {
        localStorage.setItem("auditId", audit.data.r01_id)
      }
    })
  }
}
