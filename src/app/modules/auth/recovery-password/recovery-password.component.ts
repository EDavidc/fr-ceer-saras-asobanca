import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { GeneralService } from '../../../services/general.service';
import { UtilsService } from '../../../services/utils.service';
import { HttpClientModule } from '@angular/common/http';
import { NgIf } from '@angular/common';
import { BanksService } from '../../../services/banks.service';

@Component({
  selector: 'app-recovery-password',
  standalone: true,
  imports: [
    RouterLink,
    ReactiveFormsModule,
    HttpClientModule,
    NgIf
  ],
  templateUrl: './recovery-password.component.html',
  styleUrl: './recovery-password.component.css',
  providers: [GeneralService, UtilsService, BanksService]
})
export class RecoveryPasswordComponent implements OnInit {

  formRecoveryPassword!:FormGroup;
  showSppiner =false;
  /**
   *
   */
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private _utilService: UtilsService,
    private _generalService: GeneralService,
    private _bankService: BanksService
  ) {


  }
  ngOnInit(): void {
    this.initForm();
  }
  recoveryPassword() {
    if(this.formRecoveryPassword.invalid){
      this.formRecoveryPassword.markAllAsTouched();
      return this._utilService.showAlert('Campos incompletos','Ingrese la información solicitada',"WARNING");
    }
    this.showSppiner = true;
    const obj = { email :  this.formRecoveryPassword.get('email')?.value }
    this._bankService.recoveryPassword(obj).subscribe({
      next: res => {
        if(res.status ==='OK'){
          this.showSppiner = false;
          this._utilService.showAlert('Recuperación de contraseña', 'Se ha enviado un correo a su dirección de correo para recuperar su contraseña',"SUCCESS");
          this.router.navigate(['/sign-in']);
        }else if(res.status === "NOT_FOUND"){
          this.showSppiner = false;
          this._utilService.showAlert('Recuperación de contraseña', 'El correo electrónico ingresado no existe',"ERROR");
        }else{
          this.showSppiner = false;
          this._utilService.showAlert('Recuperación de contraseña', 'Ha ocurrido un error, intentalo mas tarde',"ERROR");
        }
      },
      error: err => {
        console.log(err);
        this.showSppiner = false;
        this._utilService.showAlert('Recuperación de contraseña', 'Ha ocurrido un error, intentalo mas tarde',"ERROR");
      }
    })
  }

  initForm(){
    this.formRecoveryPassword = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }
}
