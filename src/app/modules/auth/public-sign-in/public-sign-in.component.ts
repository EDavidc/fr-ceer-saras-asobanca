import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { UtilsService } from '../../../services/utils.service';
import { HttpClientModule } from '@angular/common/http';
import { AuditService } from '../../../services/audit.service';
import { NgxMaskDirective, provideNgxMask } from 'ngx-mask';
import { NgIf } from '@angular/common';
import { CREDENTIALS_GUEST, GUEST, STATUS_OK } from '../../../data/Constants';
import { BanksService } from '../../../services/banks.service';

@Component({
  selector: 'app-public-sign-in',
  standalone: true,
  imports: [
    RouterLink,
    ReactiveFormsModule,
    HttpClientModule,
    NgxMaskDirective,
    NgIf
  ],
  providers:[
    AuditService,
    UtilsService,
    provideNgxMask(),
    BanksService,
  ],
  templateUrl: './public-sign-in.component.html',
  styleUrl: './public-sign-in.component.css'
})
export class PublicSignInComponent implements OnInit {

  generalNumber = {
    S: { pattern: new RegExp('[0-9]') },
    L: { pattern: new RegExp('[\\p{L}\\p{M}\\s]', 'u')}
  };

  formPublic!: FormGroup;
  showSppiner = false;
  /**
   *
   */
  constructor(
    private route: Router,
    private fb: FormBuilder,
    private _utilService: UtilsService,
    private _auditService: AuditService,
    private _bankService: BanksService
  ) {
  }
  ngOnInit(): void {
    this.initForm();
  }

  signIn(){
    if(this.formPublic.invalid){
      this.formPublic.markAllAsTouched();
      this.formPublic.markAsDirty();
      this._utilService.showAlert('Campos incompletos','Ingrese toda la información solicitada', 'WARNING')
      return;
    }
    this.showSppiner = true;
    const { name, lastName, email, institution } = this.formPublic.value
    const objAUdit ={
      p15_id:null,
      p01_id:null,
      r01_nombre:name,
      r01_apellido:lastName,
      r01_institucion:institution,
      r01_correo:email,
      r01_ip: localStorage.getItem('ip'),
      r01_navegador: this._auditService.detectBrowser(),
    }

    this._auditService.registerAudit(objAUdit).subscribe({
      next:(audit:any)=>{
        if(audit.status === STATUS_OK){
          this._bankService.login(CREDENTIALS_GUEST).subscribe({
            next: (user)=>{
              if(user.status === STATUS_OK){
                localStorage.setItem('auditId', audit.data.r01_id)
                localStorage.setItem('type', GUEST)
                localStorage.setItem('token',user.data.access_token);
                localStorage.setItem('username', `${name} ${lastName}`)
                localStorage.setItem('email', email);
                localStorage.setItem('p05_id', institution);
                this._utilService.showAlert('Registro Exitoso','Se ha guardado la información correctamente','SUCCESS');
                this.route.navigate(['/guides']);
              }
              this.showSppiner = false;

            }, error:(e)=>{
              console.log(e);
              this.showSppiner = false;

            }
          })
        }else{
          this._utilService.showAlert('Ha ocurrido un error','Intentelo más tarde','ERROR');
        }
      }, error: (err)=>{
        this.showSppiner = false;
        console.log(err);
        
      }
    })
  }

  initForm(){
    this.formPublic = this.fb.group({
      name:['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      institution: ['', [Validators.required]],
    });
  }
}
