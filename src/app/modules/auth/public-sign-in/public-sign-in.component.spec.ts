import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicSignInComponent } from './public-sign-in.component';

describe('PublicSignInComponent', () => {
  let component: PublicSignInComponent;
  let fixture: ComponentFixture<PublicSignInComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PublicSignInComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PublicSignInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
