import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { BanksService } from '../../../services/banks.service';
import { HttpClientModule } from '@angular/common/http';
import { NgFor, NgIf } from '@angular/common';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { UtilsService } from '../../../services/utils.service';
import { GeneralService } from '../../../services/general.service';
import { NgxMaskDirective, provideNgxMask } from 'ngx-mask';
import { EXIST } from '../../../data/Constants';
declare var $: any; // Declare jQuery

@Component({
  selector: 'app-sign-up',
  standalone: true,
  imports: [
    RouterLink,
    HttpClientModule,
    NgFor,
    ReactiveFormsModule,
    NgIf,
    NgxMaskDirective
  ],
  templateUrl: './sign-up.component.html',
  styleUrl: './sign-up.component.css',
  providers: [
    BanksService,
    GeneralService,
    provideNgxMask()
  ]
})
export class SignUpComponent implements OnInit {
  generalNumber = {
    S: { pattern: new RegExp('[0-9]') },
    L: { pattern: new RegExp('[\\p{L}\\p{M}\\s]', 'u')}
  };
  banks:any;
  formSingUp!:FormGroup;
  disableButtonSendCode = false;
  disableVerifyCode = false;
  showSppinerBtnSendCode = false;
  showSppinerBtnVerifyCode = false;
  msgUnmatched = false;
  showSppinerBtnMain = false;
  @ViewChild('selectElement') selectElement!: ElementRef;

  constructor(
    private _bankService: BanksService,
    private fb: FormBuilder,
    private route: Router,
    private _utilService: UtilsService,
    private _generalService: GeneralService
  ) { }

  ngOnInit(): void {
    this.loadData();
    this.initForm();
  }

  loadData(){
    this._utilService.showSpinner();
    this._bankService.getBanks().subscribe(res => {
      this.banks = res;
      const defaultOption = { id: '', text: 'Seleccione una opción' };
          const formattedOptions = this.banks.map((option: any) => ({
            id: option.p05_id,
            text: option.p05_ruc + ' ' + option.p05_nombre,
          }));

          this._utilService.hideSpinner();
          const allOptions = [defaultOption, ...formattedOptions]
          $(this.selectElement.nativeElement).select2({
            data: allOptions,
            placeholder: 'Seleccione una opción',
            language: {
              noResults: function() {
                return 'No se encontraron resultados';
              }
            }
          });
          // Listen for the change event and update selectedValue
          $(this.selectElement.nativeElement).on('change', (event:any) => {
            this.formSingUp.get('bank')?.setValue($(event.target).val());
          });
    });
  }

  handleBank(){

  }

  initForm(){
    this.formSingUp = this.fb.group({
      bank: [null, [Validators.required]],
      email:['', [Validators.required, Validators.email]],
      code: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
      name: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    });
  }

  async sendCodeVerification(){
    this.formSingUp.markAsDirty();
    console.log(this.formSingUp.get('bank')?.value );
    
    if(this.formSingUp.get('bank')?.value != undefined && this.formSingUp.get('email')?.value){
      const bankSelectedId = this.formSingUp.get('bank')?.value;
      const doaminBank = this.banks.filter((bank:any) => bank.p05_id === parseInt(bankSelectedId))[0].p05_dominio.replace('@','')
      const emailRegister = this.formSingUp.get('email')?.value.split('@')[1];
      if(emailRegister === doaminBank){
        this.showSppinerBtnSendCode = true;
        //TODO: CHECK IF USE EXIST
        
       this._bankService.checkIfUserExist({email: this.formSingUp.get('email')?.value}).subscribe({
        next:(user)=>{
          if(user.status === EXIST){
            this.showSppinerBtnSendCode = false;

            return this._utilService.showAlert('Verificación de usuario',user.message, 'WARNING');
          }else{
            const codeUnique = this._utilService.generateUniqueCode();
        const objSend = {
          email: this.formSingUp.get('email')?.value,
          body: btoa(`Ingrese el siguiente código de verificación en el sistema: ${codeUnique}`),
          subject: 'Código de verificación'
        }
        this._generalService.sendMail(objSend).subscribe({
          next: res => {
            this._generalService.codeValidation.next(codeUnique);
            if(res.status ==='OK'){
              this.disableButtonSendCode = true;
              this.showSppinerBtnSendCode = false;
              this._utilService.showAlert('Código enviado', 'El código se ha enviado correctamente',"SUCCESS");
            }else{
              this._utilService.showAlert('Error al enviar el código', 'Ha ocurrido un error, intentaló mas tarde',"ERROR");
              this.showSppinerBtnSendCode = false;
            }
          },
          error: err => {
            console.log(err);
            this.showSppinerBtnSendCode = false;
          }
        });
          }
        }, error: (err)=>{
          this.showSppinerBtnSendCode = false;

        }
       })
        
        
        
      }else{
        this._utilService.showAlert("Error","El correo electrónico ingresado no pertence a la entidad financiera","ERROR");
      }
      
    }else{
      this._utilService.showAlert("Campos incompletos","Ingrese los datos solicitados","WARNING");
      this.formSingUp.markAsTouched();
    }
  }

  checkCode(){
    const codeValidate = this._generalService.codeValidation.value;
    const codeInput = this.formSingUp.get('code')?.value;    
    if(codeInput){
      this.showSppinerBtnVerifyCode = true;
      if(codeInput === codeValidate){
        this.showSppinerBtnVerifyCode = false;
        this._utilService.showAlert("Código verificado","El código ingresado es correcto", "SUCCESS");
        this.disableVerifyCode = true;
        $(this.selectElement.nativeElement).select2({
          disabled: true
      });
      }else{
        this.showSppinerBtnVerifyCode = false;
        this._utilService.showAlert("Código incorrecto","El código ingresado es incorrecto","ERROR");
      }
    }else{
      this._utilService.showAlert("Campos incompletos","Ingrese los datos solicitados","WARNING");
    }
  }

  passwordMatchValidator() {
    const password = this.formSingUp.get('password')?.value;
    const confirmPassword = this.formSingUp.get('confirmPassword')?.value;
    if(password !== confirmPassword){
      this.msgUnmatched = true;
    }else{
      this.msgUnmatched = false;
    }
  }

  registerUser(){    
    if(this.formSingUp.invalid){
      this.formSingUp.markAllAsTouched();
      return this._utilService.showAlert('Campos incompletos','Ingrese la información solicitada',"WARNING");
    }

    if(this.disableVerifyCode){
      this.showSppinerBtnMain  = true;
      const formValues = this.formSingUp.value;
      const objRegister = {
        "p05_id": formValues.bank,
        "p01_nombre": formValues.name,
        "p01_apellido": formValues.lastname,
        "password": formValues.password,
        "email": formValues.email,
        "p01_celular": formValues.phoneNumber
      }
      this._bankService.register(objRegister).subscribe({
        next: res => {
          if(res.status ==='OK'){
            localStorage.removeItem('codeUnique');
            this._utilService.showAlert('Usuario registrado', 'El usuario se ha registrado correctamente',"SUCCESS");
            this.showSppinerBtnMain  = false;
            this.route.navigate(['/sign-in']);
          }else{
            this.showSppinerBtnMain  = false;
            this._utilService.showAlert('Error al registrar el usuario', 'Ha ocurrido un error, intentalo mas tarde',"ERROR");
          }
        },
        error: err => {
          this.showSppinerBtnMain  = false;
          console.log(err);
          this._utilService.showAlert('Error al registrar el usuario', 'Ha ocurrido un error, intentalo mas tarde',"ERROR");
        }
      })
    }else{
      this._utilService.showAlert('Verificar código', 'Debe verificar el código para poder registrarse',"ERROR");
    }
  }

  

}
