import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SensibilityAnalysisComponent } from './sensibility-analysis.component';

describe('SensibilityAnalysisComponent', () => {
  let component: SensibilityAnalysisComponent;
  let fixture: ComponentFixture<SensibilityAnalysisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SensibilityAnalysisComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SensibilityAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
