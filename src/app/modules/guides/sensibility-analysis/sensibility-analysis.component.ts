import { Component, OnInit } from '@angular/core';
import { FooterComponent } from '../../../shared/layout/footer/footer.component';
import { HeaderComponent } from '../../../shared/layout/header/header.component';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LocalizationService } from '../../../services/localization.service';
import { AuditService } from '../../../services/audit.service';
import { UtilsService } from '../../../services/utils.service';

import moment from "moment";
//PDF
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-sensibility-analysis',
  standalone: true,
  imports: [
    FooterComponent,
    HeaderComponent,
    ReactiveFormsModule,
    CommonModule,
    RouterLink,
  ],
  templateUrl: './sensibility-analysis.component.html',
  styleUrl: './sensibility-analysis.component.css',
  providers:[LocalizationService, AuditService, UtilsService]
})

export class SensibilityAnalysisComponent implements OnInit {
  formSensibility!:FormGroup;

  //Variables iniciales
  img = 'assets/images/background/asobanca_guias.png';
  provincias: any[]=[];
  cantones: any[]=[];
  parroquias: any[]=[];

  reporte: any[]=[];
  riesgoT: number;

  provinciaControl = new FormControl();
  cantonControl = new FormControl();
  parroquiaControl = new FormControl();

  consulta: {r01_id?:number, p01_id?:number} = {};
  ip:string="";

  constructor(
    private fb: FormBuilder,
    private route: Router,
    private _localizationService: LocalizationService,
    private _auditService: AuditService,
    private _utilService: UtilsService
  ) {}

  ngOnInit(): void {
    this._utilService.showSpinner();

    this.initForm();

    //Obtención de las provincias
    this._localizationService.getProvinces()
      .subscribe((data) => {
        this._utilService.showSpinner();
        this.provincias = data;
        this._utilService.hideSpinner();    
    });
    
    //Obtención de los Cantones de acuerdo a la Provincia seleccionada
    this.provinciaControl.valueChanges.subscribe((provinciaId) => {
      if (provinciaId) {
          //API getCantones
          this._utilService.showSpinner();
          this._localizationService.getCantons(provinciaId).subscribe((data) => {
              this.cantones = data;
              this._utilService.hideSpinner();
          })
      } 
      else {
        this.cantones = [];
      }

      //Si ya posee el dato
      if(this.provinciaControl.value){
        this.cantonControl.setValue(null);
        this.cantones = [];
        this.parroquiaControl.setValue(null);
        this.parroquias = [];
      }
    });
    
    //Obtención de las Parroquias de acuerdo al Cantón seleccionado
    this.cantonControl.valueChanges.subscribe((cantonId) => {
      if (cantonId) {
          //API getParroquias
          this._utilService.showSpinner();
          this._localizationService.getParishes(cantonId).subscribe((data) => {
            this.parroquias = data;
            this._utilService.hideSpinner();
          });
      } 
      else {
        this.parroquias = [];
      }

      //Si ya posee el dato
      if(this.cantonControl.value){
        this.parroquiaControl.setValue(null);
        this.parroquias = [];
      }
    })

    //Obtención de la latitud y longitud en caso de seleccionar las coordenadas geográficas
    this.formSensibility.get('opcionSeleccionada')?.valueChanges.subscribe((option:string)=>{
      this._utilService.showSpinner();
      if(option==='option1'){
        navigator.geolocation.getCurrentPosition(
          (position: GeolocationPosition)=>{
            this.formSensibility.patchValue({
              r01_lat: position.coords.latitude,
              r01_lon: position.coords.longitude
            });
          this._utilService.hideSpinner();
        },
        (error: GeolocationPositionError) => {
          this._utilService.showAlert('Datos de ubicación','No se pudo acceder a la ubicación...','WARNING');
          this._utilService.hideSpinner();
        }
      )}
      else{
        this.formSensibility.patchValue({
          r01_lat: null,
          r01_lon: null
        });
        this._utilService.hideSpinner();
      }
    })

    //Obtención de la dirección IP
    this.ip = sessionStorage.getItem('ip');
  }

  initForm(){
    this.formSensibility = this.fb.group({
      r01_id:[null],
      r01_lat:[null, Validators.required],
      r01_lon: [null, Validators.required],
      r01_ip:[null],
      opcionSeleccionada: [null],
      p15_id:[null],
      r01_x: [null],
      r01_y: [null],
      r01_zona_utm:[null],
      r01_update_at:[this.getFechaHoraEC()],
      r01_navegador:[this._auditService.detectBrowser()]

    });
  }

  //Función que transforma de grados a radianes
  deg2rad(degrees: number): number {
    return degrees * (Math.PI / 180);
  }
  
  //Función que transforma de coordenadas geográficas a UTM
  geoToUTM(latitude: number, longitude: number): { x: number, y: number, zone: string } {
    const earthRadius = 6378137; // Radio de la Tierra en metros
    const k0 = 0.9996; // Factor de escala de la proyección
  
    // Constantes para el cálculo
    const a = 6378137;
    const e = 0.081819191;
  
    const latRad = this.deg2rad(latitude);
    const lonRad = this.deg2rad(longitude);
  
    const lonOrigin = Math.floor((longitude + 180) / 6) * 6 - 180 + 3; // Longitud del meridiano central
    const lonOriginRad = this.deg2rad(lonOrigin);
  
    const eccPrimeSquared = (e * e) / (1 - (e * e)); // Eccentricidad al cuadrado
  
    const N = earthRadius / Math.sqrt(1 - (e * e * Math.sin(latRad) * Math.sin(latRad))); // Radio de curvatura en el plano normal
    const T = Math.tan(latRad) * Math.tan(latRad); // Tangente cuadrada de la latitud
    const C = eccPrimeSquared * Math.cos(latRad) * Math.cos(latRad); // Coeficiente de la segunda potencia de la función de expansión
  
    const A = Math.cos(latRad) * (lonRad - lonOriginRad);
    const M = earthRadius * ((1 - (e * e / 4) - (3 * e * e * e * e / 64) - (5 * e * e * e * e * e * e / 256)) * latRad - (3 * e * e / 8 + 3 * e * e * e * e / 32 + 45 * e * e * e * e * e * e / 1024) * Math.sin(2 * latRad) + (15 * e * e * e * e / 256 + 45 * e * e * e * e * e * e / 1024) * Math.sin(4 * latRad) - (35 * e * e * e * e * e * e / 3072) * Math.sin(6 * latRad));
  
    const easting = (k0 * N * (A + (1 - T + C) * (A * A * A / 6) + (5 - 18 * T + T * T + 72 * C - 58 * eccPrimeSquared) * (A * A * A * A * A / 120) + (1 - T + 2 * C) * (A * A) / 2)) + 500000; // Coordenada este (x)
  
    let northing = (k0 * (M + N * Math.tan(latRad) * (A * A / 2 + (5 - T + 9 * C + 4 * C * C) * (A * A * A * A / 24) + (61 - 58 * T + T * T + 600 * C - 330 * eccPrimeSquared) * (A * A * A * A * A * A / 720)))) // Coordenada norte (y)
    let hemisphere = latitude >= 0 ? "N" : "S";
    
    if (latitude < 0) {
      northing += 10000000; // Corrección para el hemisferio sur
    }
  
    const zone = Math.floor((longitude + 180) / 6) + 1; // Determinación de la zona UTM
  
    return { x: easting, y: northing, zone:`${zone}${hemisphere}` };
  }
  
  //Función que obtiene el código de la zona UTM
  getCodeUTM(zUTM:string){ 
    if(zUTM!=null){
        const zUTMStr: string = zUTM.toString();
        const zonaUTMMap: { [key: string]: string } = {
            "17N": "32617",
            "17S": "32717",
            "18N": "32618",
            "18S": "32718"
        };
        return zonaUTMMap[zUTMStr] || null;
    }
    return null;
  }
  
  // Función para obtener el navegador
  getNavegador(): string {
    return navigator.userAgent;
  }
  
  //Función para obtener la hora actual en la zona de Ecuador
  getFechaHoraEC(): Date{
    const ecuadorTimezoneOffset = 5 * 60 * 60 * 1000; // UTC-5
    const currentDate = new Date();
    const ecuadorTime = new Date(currentDate.getTime() - ecuadorTimezoneOffset); // Restar el desfase de zona horaria de Ecuador
    return ecuadorTime;
  }

  //Settear los valores al momento de actualizar
  updateForm():void{
    //Si se selecciona la opción 1
    if(this.formSensibility.get('opcionSeleccionada')?.value === 'option1'){
      //Settear el valor en nulo
      this.provinciaControl.setValue(null);
      this.cantonControl.setValue(null);
      this.parroquiaControl.setValue(null);
      
      //Transformación de coordenadas geográficas a UTM
      if(this.formSensibility.value.r01_lat && this.formSensibility.value.r01_lon){
        const utmCoords = this.geoToUTM(Number(this.formSensibility.value.r01_lat),Number(this.formSensibility.value.r01_lon));
        this.formSensibility.value.r01_x = utmCoords.x;
        this.formSensibility.value.r01_y = utmCoords.y;
        this.formSensibility.value.r01_zona_utm = this.getCodeUTM(utmCoords.zone);
      }
      else{
        this.formSensibility.value.r01_x = null;
        this.formSensibility.value.r01_y = null;
        this.formSensibility.value.r01_zona_utm = null;

        //Settear el valor en nulo
        this.formSensibility.get('r01_lat')?.setValue(null);
        this.formSensibility.get('r01_lon')?.setValue(null);
      }
    }
    //Si no se selecciona
    else{
      this.formSensibility.value.r01_x = null;
      this.formSensibility.value.r01_y = null;
      this.formSensibility.value.r01_zona_utm = null;

      //Settear el valor en nulo
      this.formSensibility.get('r01_lat')?.setValue(null);
      this.formSensibility.get('r01_lon')?.setValue(null);
    }
  }
  
  //Guarda el registro
  save(){
    this._utilService.showSpinner();

    //Actualización de datos
    this.updateForm();
    this.formSensibility.value.r01_id = Number(localStorage.getItem('auditId'));

    if(this.consulta.p01_id){
      this.formSensibility.value.r01_update_by = this.consulta.p01_id;
    }
    this.formSensibility.value.r01_ip = this.ip;
    this.formSensibility.value.p15_id = this.parroquiaControl.value;

    //Si no se selecciona ninguna opción
    if(this.formSensibility.value.opcionSeleccionada === null){
      this._utilService.showAlert('Información','Debe seleccionar una de las opciones','INFO');
      this._utilService.hideSpinner();
      return
    }

    //Si los datos no están correctos
    if((this.formSensibility.value.r01_lat ===null || this.formSensibility.value.r01_lat ==='' || 
        this.formSensibility.value.r01_lon===null || this.formSensibility.value.r01_lon ==='') && this.formSensibility.value.p15_id===null){
      this.formSensibility.markAllAsTouched();
      this.formSensibility.markAsDirty();
      this._utilService.showAlert('Campos incompletos','Ingrese toda la información solicitada', 'WARNING')
      this._utilService.hideSpinner();
      return;
    }

    //Envía los datos
    this._auditService.putRegister(this.formSensibility.value).subscribe({
      next:(audit:any)=>{
        if(audit.status ==='OK'){
          //Mensaje del registro
          this._utilService.showAlert('Registro Exitoso','Se ha guardado la información correctamente','SUCCESS');
          this._utilService.showAlert('Generando informe','Espere un momento...','INFO');

          //Generación del reporte
          this._auditService.generateReport(this.formSensibility.value.r01_id).subscribe({
            next:(report:any)=>{
              if(report.status === 'OK'){
                //Mensaje del Reporte
                this._utilService.showAlert('Generando informe','Cargando la información...','INFO');
                
                //Obtención del reporte
                this._auditService.getReport(this.formSensibility.value.r01_id).subscribe((data)=>{
                  this.reporte = data;
                  this._utilService.showAlert('Informe Exitoso','Información generada correctamente','SUCCESS');

                  //Obtención del Riesgo Territorial
                  this._auditService.getRiesgoTerritorial(this.formSensibility.value.r01_id).subscribe(async (data)=>{
                    this.riesgoT = Number(data[0].r02_riesgo_territorial);
                    await this.generatePDF();
                    this._utilService.hideSpinner();
                  })
                  
                  this._utilService.hideSpinner();
                })

                this._utilService.hideSpinner();
              }
              else{
                this._utilService.showAlert('Ha ocurrido un error con el informe','Intentelo más tarde','ERROR');
                this._utilService.hideSpinner();
              }
            }
          })
        }else{
          this._utilService.showAlert('Ha ocurrido un error','Intentelo más tarde','ERROR');
          this._utilService.hideSpinner();
        }
      }
    })
  }

  //Generación del Informe de la Orden de Trabajo en formato PDF
  async generatePDF() {
    //Generación de la fecha y hora
    var fechaHora = this.formSensibility.value.r01_update_at;
    var fecha = moment(new Date(fechaHora)).format("YYYY-MM-DD");
    var hora = moment(new Date(fechaHora)).format("HH:mm:ss");

    //Imagen Header
    const headerimg = '../assets/images/header_riesgoMapas.png';
    const base64HeaderImage = await this.getBase64Image(headerimg);

    //Imagen Footer
    const footerimg = '../assets/images/footer_riesgoMapas.png';
    const base64FooterImage = await this.getBase64Image(footerimg);

    // Genera el contenido para la tabla con colores según el riesgo
    const content = this.reporte.map((item: any, index: number) => [
        { text: '\n'+item.r03_nombre || '', bold: true, fillColor: index % 2 === 0 ? "#FFFFFF":"#F8F6F6", fontSize: 10, border: [], marginLeft: 10 },
        { canvas: [{ type: 'rect', x: 10, y: 8, w: 12, h: 12, r: 0, color: this.getColorByRisk(item.r03_riesgo) }], fillColor: index % 2 === 0 ? "#FFFFFF":"#F8F6F6", alignment: 'center', margin: [2, 2, 2, 2], border: [] },
        { text: [ //Condicional para la primera parte de la redacción
                (item.r03_nombre.includes('Áreas'))?{ text: "La afectación a las "}:
                (item.r03_nombre.includes('Amenaza'))?{text:"La "}:
                (item.r03_nombre.includes('Tsunami'))?{text:"La amenaza de "}:
                (item.r03_nombre.includes('Peligros'))?{text:"La amenaza por "}:
                (item.r03_nombre.includes('Reserva'))?{text:"La afectación a una "}:
                (item.r03_nombre.includes('Zona'))?{text:"La afectación a la "}:
                (item.r03_nombre.includes('Sitios') || item.r03_nombre.includes('Territorios'))?{text:"La afectación a los "}:
                (item.r03_nombre.includes('Sequías'))?{text:"La afectación por "}:{text:"La afectación al "},
                    { text: item.r03_nombre, bold: true },
                    { text: " es " }, 
                    { text: this.getNivel(item.r03_riesgo), bold: true, color: this.getColorByRisk(item.r03_riesgo) },
                    { text: "\nFuente: ", bold: true},
                    { text: item.a04_fuente}], 
                fillColor: index % 2 === 0 ? "#FFFFFF":"#F8F6F6", fontSize: 9, border: [], alignment:"justify", marginLeft: 10}
    ]);

    let docDefinition = {
        info: {
            title: "Reporte Sensibilidad Territorial - ASOBANCA",
            author: "CEER",
            subject: "subject of document",
            keywords: "keywords for document",
        },
        content: [
            //Header
            {
                image: base64HeaderImage,
                width: 520,
                alignment: 'center',
                margin: [0, -35, 0, 0],
            },
            { columns: [{}, { text: "\n" }] },
            //Contenido de la tabla
            {
                table: {
                    widths: ['*', 'auto', 'auto'], // Ajusta los anchos de las columnas según tus necesidades
                    body: [
                        [
                            { text: "MAPA", bold: true, color: "#FFFFFF", fillColor: "#2a5291", alignment: 'center', fontSize: 11, border: []},
                            { text: "RIESGO", bold: true, color: "#FFFFFF", fillColor: "#2a5291", alignment: 'center', fontSize: 11, border: []},
                            { text: "RECOMENDACIONES", bold: true, color: "#FFFFFF", fillColor: "#2a5291", alignment: 'center', fontSize: 11, border: [] }
                        ],
                        ...content                            
                    ],
                },
            },
            { columns: [{}, { text: "\n" }] },
            { columns: [{}, { text: "\n" }] },
            {
              table: {
                  widths: ['*', 'auto', '*'], // Ajusta los anchos de las columnas según tus necesidades
                  body: [
                      [
                        { text: "RIESGO TERRITORIAL:", bold:true, color:"#2a5291", fontSize:12, marginLeft: 95, border: []},
                        { canvas: [{ type: 'rect', x: 0, y: 0, w: 12, h: 12, r: 0, color: this.getColorByRisk(this.riesgoT) }], border: []},
                        { text: this.getNivelM(this.riesgoT), bold: true, color: this.getColorByRisk(this.riesgoT), fontSize:12, border: [] },
                      ],                           
                  ],
              },
            },
              
        ],
        footer: {image: base64FooterImage, width: 515, alignment: 'center', margin: [20, -22, 0, 20],}
    };

    //Crea el PDF
    pdfMake.createPdf(docDefinition).download("Reporte_Sens_Territorial_ASOBANCA.pdf");
}

//Función que transforma las imágenes a base64 (bits)
getBase64Image(url:any) {
    return new Promise((resolve, reject) => {
    var img = new Image();
    img.setAttribute("crossOrigin", "anonymous");
    img.onload = () => {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx?.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        resolve(dataURL);
    };
    img.onerror = (error) => {reject(error);};
    img.src = url;
    });
}
//Función para obtener el color del riesgo
getColorByRisk(riesgo: number): string {
  const result = riesgo.toString();
  switch (result) {
      case '3':
          return "#a21a17"; // Rojo
      case '2':
          return "#ffcc00"; // Amarillo
      case '1':
          return "#afca0b"; // Verde
      default:
          return "#FFFFFF"; // Blanco por defecto
  }
}

  //Función para obtener el color del riesgo
  getNivel(riesgo: number): string {
    const result = riesgo.toString();
    switch (result) {
        case '3':
            return "ALTA"; // Rojo
        case '2':
            return "MEDIA"; // Amarillo
        case '1':
            return "BAJA"; // Verde
        default:
            return "NO APLICA"; // Blanco por defecto
    }
  }

  //Función para obtener el color del riesgo
  getNivelM(riesgo: number): string {
    const result = riesgo.toString();
    switch (result) {
        case '3':
            return "ALTO"; // Rojo
        case '2':
            return "MEDIO"; // Amarillo
        case '1':
            return "BAJO"; // Verde
        default:
            return "NO APLICA"; // Blanco por defecto
    }
  }
}
