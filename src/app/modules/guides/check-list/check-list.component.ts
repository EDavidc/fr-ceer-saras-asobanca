import { Component, OnInit } from '@angular/core';
import { FooterComponent } from '../../../shared/layout/footer/footer.component';
import { HeaderComponent } from '../../../shared/layout/header/header.component';
import { NgFor, NgIf, CommonModule } from '@angular/common';
import { Router, RouterLink, ActivatedRoute, Route } from '@angular/router';
import { GuidingquestionService } from '../../../services/guidingquestion.service';
import { SectionsService } from '../../../services/sections.service';
import { ListaddService } from '../../../services/listadd.service';
import { UtilsService } from '../../../services/utils.service';
import { AuditService } from '../../../services/audit.service';
import { GuidesService } from '../../../services/guides.service';

//PDF
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-check-list',
  standalone: true,
  imports: [
    FooterComponent,
    HeaderComponent,
    CommonModule,
    NgFor,
    RouterLink,
    NgIf
  ],
  templateUrl: './check-list.component.html',
  styleUrl: './check-list.component.css',
  providers: [
    SectionsService,
    GuidingquestionService,
    ListaddService,
    UtilsService,
    AuditService,
    GuidesService
  ],
})
export class CheckListComponent implements OnInit {

  img = 'assets/images/background/asobanca_guias.png';

  constructor(
    private _router: Router,
    private _guidingquestionService: GuidingquestionService,
    private _ListaddService: ListaddService,
    private activeRoute: ActivatedRoute,
    private _utilService: UtilsService,
    private _guidesService: GuidesService
  ) {
  }
  ngOnInit(): void {
    this._utilService.showSpinner();
    this.activeRoute.paramMap.subscribe({
      next: (val: any) => {
        this.filterquestion(val.params.guideId);
      }
    })
  }

  catalogQuestion: any;
  questionSelect: any = [];
  cuestionario: any;
  consulta: { r01_id?: number, p01_id?: number } = {};

  filterquestion(idguia: any): any {
    this._guidingquestionService.getGuidingQuestionById(idguia)
      .subscribe((data1) => {
        if (data1.status === 'OK') {
          this.catalogQuestion = data1.data;
          this._utilService.hideSpinner();
        }
      }
      )
  }

  save() {
    this._utilService.showSpinner();
    //Guarda las respuestas
    if (this.questionSelect.length > 0) {

      let arrayquestion = new Array();
    this.consulta.r01_id = Number(localStorage.getItem('auditId'));
    this.questionSelect.forEach((element: any) => {
      arrayquestion.push({
        "p02_id": element,
        "r01_id": this.consulta.r01_id,
        "r04_estado": 1,
        "r04_create_by": 1
      });
    });
    var ssss = JSON.stringify(arrayquestion);
    this._ListaddService.register(ssss).subscribe((data) => {
      if (data.status === 'OK') {
        this._utilService.showAlert('Preguntas registradas!!', '', 'SUCCESS');
        this._utilService.showAlert('Generando informe', 'Espere un momento...', 'INFO');
  
        //Obtiene las preguntas y respuestas del cuestionario
        this._guidesService.getCuestionario(this.consulta.r01_id).subscribe(async (data) => {
          this._utilService.showAlert('Informe Exitoso', 'Información generada correctamente', 'SUCCESS');
          if (data.status === 'OK') {
            this.cuestionario = data.data;
            await this.generatePDF();
            this._utilService.hideSpinner();
            this._router.navigate(['/guide-information']);
          }
        })
       }
    })

      
    } else {
      this._utilService.hideSpinner();
      return this._utilService.showAlert('Ninguna pregunta seleccionada', '', 'WARNING');
    }

  }

  checkValue(id: number, val: any) {
    const d = this.questionSelect.indexOf(id);
    if (d !== -1) {
      this.questionSelect.splice(d, 1);
    }
    if (val.checked && val.value == "0") {
      this.questionSelect.push(id);
    }
  }

  async dataSave() {
    
  }

  //Generación del Informe de la Orden de Trabajo en formato PDF
  async generatePDF() {
    //Imagen Header
    const headerimg = '../assets/images/header_evaluacionRiesgos.png';
    const base64HeaderImage = await this.getBase64Image(headerimg);

    //Imagen Footer
    const footerimg = '../assets/images/footer_riesgoMapas.png';
    const base64FooterImage = await this.getBase64Image(footerimg);

    // Genera el contenido para la tabla con colores según el riesgo
    const content = this.cuestionario[0].map((item: any, index: number) => [
      { text: '\n' + item.PREGUNTA + '\n' || '', bold: true, fillColor: index % 2 === 0 ? "#FFFFFF" : "#F8F6F6", alignment: 'justify', fontSize: 9, border: [], marginLeft: 5 },
      { text: '\n' + item.RECOMENDACION + '\n' || '', bold: true, fillColor: index % 2 === 0 ? "#FFFFFF" : "#F8F6F6", alignment: 'justify', fontSize: 9, border: [], marginLeft: 10 }
    ]);

    const content1 = this.cuestionario[1].map((item: any, index: number) => [
      { text: '\n' + item.PREGUNTA + '\n' || '', bold: true, fillColor: index % 2 === 0 ? "#FFFFFF" : "#F8F6F6", alignment: 'justify', fontSize: 9, border: [], marginLeft: 5 },
      { text: '\n' + item.RECOMENDACION + '\n' || '', bold: true, fillColor: index % 2 === 0 ? "#FFFFFF" : "#F8F6F6", alignment: 'justify', fontSize: 9, border: [], marginLeft: 10 }
    ]);

    const content2 = this.cuestionario[2].map((item: any, index: number) => [
      { text: '\n' + item.PREGUNTA + '\n' || '', bold: true, fillColor: index % 2 === 0 ? "#FFFFFF" : "#F8F6F6", alignment: 'justify', fontSize: 9, border: [], marginLeft: 5 },
      { text: '\n' + item.RECOMENDACION + '\n' || '', bold: true, fillColor: index % 2 === 0 ? "#FFFFFF" : "#F8F6F6", alignment: 'justify', fontSize: 9, border: [], marginLeft: 10 }
    ]);

    let docDefinition = {
      info: {
        title: "Reporte de Riesgos Ambientales, Laborales y Sociales - ASOBANCA",
        author: "CEER",
        subject: "subject of document",
        keywords: "keywords for document",
      },
      content: [
        //Header
        {
          image: base64HeaderImage,
          width: 520,
          alignment: 'center',
          margin: [0, -35, 0, 0],
        },
        { columns: [{}, { text: "\n" }] },
        //Contenido de la tabla
        ...(content.length > 0 ? [
          { text: "RIESGOS AMBIENTALES\n\n", bold: true, color: "#2a5291", fontSize: 12 },
          {
            table: {
              widths: ['*', '*'], // Ajusta los anchos de las columnas según tus necesidades
              body: [
                [
                  { text: "PREGUNTAS", bold: true, color: "#FFFFFF", fillColor: "#2a5291", alignment: 'center', fontSize: 10, border: [] },
                  { text: "RECOMENDACIONES", bold: true, color: "#FFFFFF", fillColor: "#2a5291", alignment: 'center', fontSize: 10, border: [] },
                ],
                ...content
              ],
            },
          }
        ] : []),
        { columns: [{}, { text: "\n" }] },
        ...(content1.length > 0 ? [
          { text: "RIESGOS LABORALES\n\n", bold: true, color: "#2a5291", fontSize: 12 },
          {
            table: {
              widths: ['*', '*'], // Ajusta los anchos de las columnas según tus necesidades
              body: [
                [
                  { text: "PREGUNTAS", bold: true, color: "#FFFFFF", fillColor: "#2a5291", alignment: 'center', fontSize: 10, border: [] },
                  { text: "RECOMENDACIONES", bold: true, color: "#FFFFFF", fillColor: "#2a5291", alignment: 'center', fontSize: 10, border: [] },
                ],
                ...content1
              ],
            },
          }
        ] : []),
        { columns: [{}, { text: "\n" }] },
        ...(content2.length > 0 ? [
          { text: "RIESGOS SOCIALES\n\n", bold: true, color: "#2a5291", fontSize: 12 },
          {
            table: {
              widths: ['*', '*'], // Ajusta los anchos de las columnas según tus necesidades
              body: [
                [
                  { text: "PREGUNTAS", bold: true, color: "#FFFFFF", fillColor: "#2a5291", alignment: 'center', fontSize: 10, border: [] },
                  { text: "RESPUESTAS", bold: true, color: "#FFFFFF", fillColor: "#2a5291", alignment: 'center', fontSize: 10, border: [] },
                ],
                ...content2
              ],
            },
          }
        ] : [])
      ],
      footer: (currentPage, pageCount) => {
        // Agregar la imagen solo en la última página
        if (currentPage === pageCount) {
          return {
            image: base64FooterImage,
            width: 515,
            alignment: 'center',
            margin: [20, -22, 0, 20]
          };
        } else {
          return null;
        }
      }

    };

    //Crea el PDF
    pdfMake.createPdf(docDefinition).download("Reporte_Riesgos_ASOBANCA.pdf");
  }

  //Función que transforma las imágenes a base64 (bits)
  getBase64Image(url: any) {
    return new Promise((resolve, reject) => {
      var img = new Image();
      img.setAttribute("crossOrigin", "anonymous");
      img.onload = () => {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx?.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        resolve(dataURL);
      };
      img.onerror = (error) => { reject(error); };
      img.src = url;
    });
  }
}