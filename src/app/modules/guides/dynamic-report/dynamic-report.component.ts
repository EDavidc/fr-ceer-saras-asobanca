import { CommonModule, NgFor } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { GuidesService } from '../../../services/guides.service';
import { UtilsService } from '../../../services/utils.service';
import { FooterComponent } from '../../../shared/layout/footer/footer.component';
import { HeaderComponent } from '../../../shared/layout/header/header.component';
import { ModalFactEnvComponent } from '../../../shared/components/modal-fact-env/modal-fact-env.component';
import { IconComponent } from '../../../shared/components/icon/icon.component';

declare var $: any; // Declare jQuery to avoid TypeScript errors

@Component({
  selector: 'app-dynamic-report',
  standalone: true,
  imports: [
    CommonModule,
    FooterComponent,
    HeaderComponent,
    IconComponent,
    RouterLink,
    HttpClientModule,
    NgFor,
    ModalFactEnvComponent
  ],
  templateUrl: './dynamic-report.component.html',
  styleUrl: './dynamic-report.component.css',
  providers: [GuidesService, UtilsService]
})
export class DynamicReportComponent implements OnInit {
  
  infoReport: any;
  mainUrl = 'https://asobancaadministrativa.com.asobancaguias.com/document/icons/';
  myBackgroundColor = '#ffcc00';
  titleGuide = "";
  titleSelected="";
  descriptionSelected="";

  /**
   *
   */
  constructor(
    private _guideService: GuidesService,
    private activeRoute: ActivatedRoute,
    private _utilService: UtilsService,
    private router:Router
  ) {
  }

  ngOnInit(): void {
    this.activeRoute.paramMap.subscribe({
      next:(val:any)=>{
        this.getReport(val.params.guideId);
      }
    })
  }

  getReport(guideId:number){
    
    this._utilService.showSpinner();
    this._guideService.getGuideById(guideId).subscribe({
      next:guide=>{
        if(guide.status === 'OK'){
          this.titleGuide = guide.data.p06_nombre;
        }
      }
    })
    this._guideService.getInformationDynamicReport(guideId).subscribe({
      next: (info)=>{
        if(info.status === 'OK'){
          this.infoReport = info.data;
        }
        this._utilService.hideSpinner();
      }
    })
  }

  showModal(process:string, title:string){
    this.titleSelected = title;
    this.descriptionSelected = process;
    if(this.descriptionSelected !== ''){
      $('#emergentModal').modal('show');
    }else{
      this._utilService.showAlert('Alertas','No existen alertas en el proceso','info')
    }
  }

  backPage(){
    this.router.navigateByUrl('/guide-information');
  }
}
