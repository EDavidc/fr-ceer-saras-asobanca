import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FooterComponent } from '../../../shared/layout/footer/footer.component';
import { HeaderComponent } from '../../../shared/layout/header/header.component';
import { NgFor, NgIf } from '@angular/common';
import { Router, RouterLink } from '@angular/router';
import { GuidesService } from '../../../services/guides.service';
import { UtilsService } from '../../../services/utils.service';
import { AuditService } from '../../../services/audit.service';
declare var $: any; // Declare jQuery

@Component({
  selector: 'app-guide-information',
  standalone: true,
  imports: [
    FooterComponent,
    HeaderComponent,
    NgFor,
    RouterLink,
    NgIf
  ],
  templateUrl: './guide-information.component.html',
  styleUrl: './guide-information.component.css',
  providers: [GuidesService]
})
export class GuideInformationComponent implements OnInit {

  showSppinerGuideDownload = false;
  showSppinerDocDownload = false;
  @ViewChild('selectElement') selectElement!: ElementRef;

  /**
   *
   */
  constructor(
    private _guidesService: GuidesService,
    private router: Router,
    private _utilService: UtilsService,
    private _auditService: AuditService
  ) { }

  ngOnInit(): void {

    this.getGuides();

  }

  catalogGuides: any;
  guideSelected!: number;
  guides: any = [
    {
      id: 1,
      url: '/static-report',
      image: 'assets/images/background/RAmbientales.png',
      title: 'Aspectos Ambientales',
      type: 'static'
    },
    {
      id: 2,
      url: '/static-report',
      image: 'assets/images/background/RLaborales.png',
      title: 'Riesgos Laborales',
      type: 'static'
    },
    {
      id: 3,
      url: '/static-report',
      image: 'assets/images/background/RSociales.png',
      title: 'Riesgos Sociales',
      type: 'static'
    },
    {
      id: 4,
      url: '/dynamic-report',
      image: 'assets/images/background/Procesos.png',
      title: 'Procesos',
      type: 'dynamic'
    },
    {
      id: 5,
      url: '#',
      image: 'assets/images/background/Documentos.png',
      title: 'Documentos Habilitantes',
      type: 'none'
    },
    {
      id: 6,
      url: '/check-list',
      image: 'assets/images/background/Check-List.png',
      title: 'Check List Visita Empresa',
      type: 'static'
    }
  ];

  downloadGuide() {
    if (!this.guideSelected) {
      this._utilService.showAlert('Guía no seleccionada', 'Seleccione una guía para descargar', 'WARNING')
    }
    this.showSppinerGuideDownload = true;
    this.insertAudit(0,true);
    this._guidesService.getPathDownloadGuide(this.guideSelected).subscribe({
      next: data => {
        if (data.status === "OK") {
          window.open(data.data.url);
        }
        this.showSppinerGuideDownload = false;
      },
      error: error => {
        console.log(error);
        this.showSppinerGuideDownload = false;
      }
    })

  }

  redirGuide(obj: any) {
    if (obj.type === 'static') {
      if (!this.guideSelected) {
        return this._utilService.showAlert('Guía no seleccionada', 'Seleccione una guía para continuar', 'WARNING');
      }
      if (!this.guideSelected) {
        return this._utilService.showAlert('Configuración vacía', 'No se ha configurado parámetros de reporte', 'WARNING');
      }
      this.insertAudit(obj.id, false);
      this.router.navigate([obj.url, this.guideSelected, obj.id]);

    } else if (obj.type === 'dynamic') {
      if (!this.guideSelected) {
        return this._utilService.showAlert('Guía no seleccionada', 'Seleccione una guía para continuar', 'WARNING');
      }
      this.insertAudit(obj.id, false);
      this.router.navigate([obj.url, this.guideSelected]);

    }
    else{
      this.showSppinerDocDownload = true;
      this.insertAudit(obj.id, false);
      this._guidesService.getPathDownloadDoc(this.guideSelected).subscribe({
        next: data => {
          if (data.status === "OK") {
            window.open(data.data.url);
          }
          this.showSppinerDocDownload = false;
        },
        error: error => {
          console.log(error);
          this.showSppinerDocDownload = false;
        }
      })
    }

  }

  backPage() {

  }

  //TODO: put in resolver
  getGuides() {
    this._utilService.showSpinner();
    this._guidesService.getAllGuides().subscribe({
      next: data => {
        if (data.status === 'OK') {
          this.catalogGuides = data.data;
          const defaultOption = { id: '', text: 'Seleccione una opción' };
          const formattedOptions = this.catalogGuides.map((option: any) => ({
            id: option.p06_id,
            text: option.p06_nombre,
          }));
          //selected guideId
          const allOptions = [defaultOption, ...formattedOptions]
          $(this.selectElement.nativeElement).select2({
            data: allOptions,
            placeholder: 'Seleccione una opción',
            language: {
              noResults: function() {
                return 'No se encontraron resultados';
              }
            }
          });

          const guideId = localStorage.getItem('guideId')
          if(guideId){
              $(this.selectElement.nativeElement).val(parseInt(guideId)).trigger('change.select2');
              this.guideSelected = parseInt(guideId);
          }
          
          
          // Listen for the change event and update selectedValue
          $(this.selectElement.nativeElement).on('change', (event:any) => {
            this.guideSelected = $(event.target).val();
            localStorage.setItem('guideId', this.guideSelected.toString());
          });
        }
        this._utilService.hideSpinner();
      },
      error: error => {
        this._utilService.hideSpinner();
        console.log(error);
      }
    })
  }

  guideSelectChange(eve: any) {
    this.guideSelected = eve.target.value;
  }

  ngAfterViewInit(): void {
    $(this.selectElement.nativeElement).trigger('change');
  }

  insertAudit(id:number, dwl?:boolean) {
    const objAUdit = {
      p15_id: null,
      p01_id: localStorage.getItem('p01_id'),
      r01_nombre: localStorage.getItem('username')?.split(' ')[0],
      r01_apellido: localStorage.getItem('username')?.split(' ')[1],
      r01_institucion: localStorage.getItem('p05_id'),
      r01_correo: localStorage.getItem('email'),
      r01_ip: sessionStorage.getItem('ip'),
      r01_navegador: this._auditService.detectBrowser(),
      p06_id: this.guideSelected,
      r01_descarga_guia:dwl ? 1 : 0,
      r01_dim_ambiental:id===1 ? 1 : 0,
      r01_dim_laboral:id===2 ? 1 : 0,
      r01_dim_social:id===3 ? 1 : 0,
      r01_procesos:id===4 ? 1 : 0,
      r01_docshabilitantes:id===5 ? 1 : 0,
      r01_checklist:id===6 ? 1 : 0,
    }    
    this._auditService.registerAudit(objAUdit).subscribe({
      next: (audit: any) => {
        localStorage.setItem("auditId", audit.data.r01_id)
      }
    })
  }
}
