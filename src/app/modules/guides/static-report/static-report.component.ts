import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { FooterComponent } from '../../../shared/layout/footer/footer.component';
import { HeaderComponent } from '../../../shared/layout/header/header.component';
import { GuidesService } from '../../../services/guides.service';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule, NgFor } from '@angular/common';
import { UtilsService } from '../../../services/utils.service';
import { subscribe } from 'node:diagnostics_channel';

@Component({
  selector: 'app-static-report',
  standalone: true,
  imports: [
    CommonModule,
    FooterComponent,
    HeaderComponent,
    RouterLink,
    HttpClientModule,
    NgFor
  ],
  templateUrl: './static-report.component.html',
  styleUrl: './static-report.component.css',
  providers: [GuidesService, UtilsService]
})
export class StaticReportComponent implements OnInit {
  infoReport: any;
  mainUrl = 'https://asobancaadministrativa.com.asobancaguias.com/document/icons/';
  myBackgroundColor = '#ffcc00';
  titleGuide = "";
  subtitleGuide  = "";
  dimensionId = 0;
  /**
   *
   */
  constructor(
    private _guideService: GuidesService,
    private activeRoute: ActivatedRoute,
    private _utilService: UtilsService,
    private router: Router
  ) {
  }
  ngOnInit(): void {
    this.activeRoute.paramMap.subscribe({
      next:(val:any)=>{
        this.dimensionId = parseInt(val.params.dimensionId);
        this.getReport(val.params.guideId,val.params.dimensionId);
      }
    })
  }

  getReport(guideId:number,dimensionId:number){
    this._utilService.showSpinner();
    this._guideService.getTititleStaticreport(guideId,dimensionId).subscribe({
      next:guide=>{
        if(guide.status === 'OK'){
          this.titleGuide = guide.data.result.split("-")[0];
          this.subtitleGuide = guide.data.result.split("-")[1];
        }
      }
    })
    this._guideService.getInformationStaticReport(guideId,dimensionId).subscribe({
      next: data => {
        console.log(data);
        if(data.status ==="OK"){
          this.infoReport = data.data;
          if(data.data.length==0){
            this._utilService.showAlert('No se encontró información','No existe información para la guía y dimensión seleccionada', 'WARNING')
          }
        }

        this._utilService.hideSpinner();
      },
      error: error => {

        this._utilService.hideSpinner();
        console.log(error);
      }
    })
  }

  backPage(){
    this.router.navigateByUrl('/guide-information');
  }
}
