import { Component } from '@angular/core';
import { FooterComponent } from '../../../shared/layout/footer/footer.component';
import { HeaderComponent } from '../../../shared/layout/header/header.component';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
  FooterComponent,
  HeaderComponent,
  RouterLink
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {

  img = 'assets/images/background/asobanca_guias.png';
  redirGuidesInformation(){

  }
  redirAnalysis(){

  }
  backPage(){
    
  }
}
