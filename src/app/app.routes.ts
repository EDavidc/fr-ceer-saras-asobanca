import { DynamicReportComponent } from './modules/guides/dynamic-report/dynamic-report.component';
import { Routes } from '@angular/router';

import { HomeComponent as GuideHome } from './modules/guides/home/home.component';
import { SignInComponent } from './modules/auth/sign-in/sign-in.component';
import { SignUpComponent } from './modules/auth/sign-up/sign-up.component';
import { GuideInformationComponent } from './modules/guides/guide-information/guide-information.component';
import { HomeComponent as MainHome } from './modules/main/home/home.component';
import { RecoveryPasswordComponent } from './modules/auth/recovery-password/recovery-password.component';
import { PublicSignInComponent } from './modules/auth/public-sign-in/public-sign-in.component';
import { StaticReportComponent } from './modules/guides/static-report/static-report.component';
import { GuidingquestionComponent } from './modules/guidingquestion/guidingquestion.component';
import { CheckListComponent} from './modules/guides/check-list/check-list.component';
import { SensibilityAnalysisComponent } from './modules/guides/sensibility-analysis/sensibility-analysis.component';
import { authGuard } from './guards/auth.guard';
import { termsGuard } from './guards/terms.guard';

export const routes: Routes = [
    {
        path: '',
        component: MainHome
    },
    {
        path: 'sign-in',
        component: SignInComponent,
        // canActivate:[termsGuard]
    },
    {
        path: 'public-sign-in',
        component: PublicSignInComponent,
        // canActivate:[termsGuard]
    },
    {
        path:'sign-up',
        component: SignUpComponent
    },
    {
        path: 'recovery-password',
        component: RecoveryPasswordComponent,
        // canActivate:[termsGuard]
    },
    {
        path:'guides',
        component: GuideHome
    },
    {
        path:'guide-information',
        component: GuideInformationComponent
    },
    {
        path:'sensibility-analysis',
        component: SensibilityAnalysisComponent
    },
    {
        path:'static-report',
        component: StaticReportComponent,
        canActivate: [authGuard]
    },
    {
        path:'static-report/:guideId',
        component: StaticReportComponent
    },
    {
        path:'static-report/:guideId/:dimensionId',
        component: StaticReportComponent
    }, 
    {
        path:'dynamic-report',
        component: StaticReportComponent
    }, 
    {
        path:'dynamic-report/:guideId',
        component: DynamicReportComponent
    },
    {
        path:'guidingquestion',
        component: GuidingquestionComponent
    },
    {
        path:'check-list/:guideId/:dimensionId',
        component: CheckListComponent
    }
];
