import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from "rxjs/operators";
import { UtilsService } from '../services/utils.service';
import { Router } from '@angular/router';

@Injectable()
export class ErrorCatchingInterceptor implements HttpInterceptor {

    constructor(
      private _utilService : UtilsService,
      private router: Router
    ) {
    }

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        return next.handle(request)
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    if (error.error instanceof ErrorEvent) {
                        this._utilService.showAlert('Ha ocurrido un error',error.error.message, 'ERROR');
                    } else {
                        switch(error.status){
                          case 500:
                            this._utilService.showAlert('Ha ocurrido un error','Los servicios no han respondido de manera correcta.', 'ERROR');
                            break;
                          case 422:
                            this._utilService.showAlert('No se puede procesar','Verfique la información enviada.', 'ERROR');
                            break;
                          case 401:
                            this._utilService.showAlert('No autorizado','Inicie sesión nuevamente.', 'ERROR');
                            this.router.navigate(['/']);
                            localStorage.clear();
                            break;
                          default:
                            this._utilService.showAlert('Ha ocurrido un error inesperado','Si el error persiste comuniquese con el administrador.', 'ERROR');
                            this.router.navigate(['/']);
                            localStorage.clear();
                            break;
                        }
                    }
                    this._utilService.hideSpinner();
                    return throwError("Error: some error in app or server");
                })
            )
    }
}