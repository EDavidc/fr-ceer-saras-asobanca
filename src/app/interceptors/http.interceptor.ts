import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class BasicInterceptor implements HttpInterceptor {
  constructor() { }
  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (req.url.includes('api.ipify.org') || req.url.includes('api64.ipify.org')) {
      return next.handle(req);
    }
    const token = localStorage.getItem('token')
    if (token) {
      const clone = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      })
      return next.handle(clone)
    } else {
      return next.handle(req)
    }

  }
}